<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><? echo $title;?></title>
	<link rel="stylesheet" href="/font/proxima/stylesheet.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="/css/main.css?v=1502">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
</head>
<body>

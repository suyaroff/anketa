<h2>План действий</h2>
<ul class="plan-list">
	<li><a href="/doc/step1" class="<? if ($this->router->method > 'step1') echo 'text-success';
		if ($this->router->method == 'step1') echo 'current-stp'; ?>">1. Заполнить реквизиты</a></li>
	<li><a href="/doc/step2" class="<? if ($this->router->method > 'step2') echo 'text-success';
		if ($this->router->method == 'step2') echo 'current-stp'; ?>">2. Указать ключевых работников</a></li>
	<li><a href="/doc/step3" class="<? if ($this->router->method > 'step3') echo 'text-success';
		if ($this->router->method == 'step3') echo 'current-stp'; ?>">3. Указать виды физических лиц</a></li>
	<li><a href="/doc/step4" class="<? if ($this->router->method > 'step4') echo 'text-success';
		if ($this->router->method == 'step4') echo 'current-stp'; ?>">4. Указать цели получения данных</a></li>
	<li><a href="/doc/step5" class="<? if ($this->router->method > 'step5') echo 'text-success';
		if ($this->router->method == 'step5') echo 'current-stp'; ?>">5. Указать третьи стороны</a></li>
	<li><a href="/doc/step6" class="<? if ($this->router->method > 'step6') echo 'text-success';
		if ($this->router->method == 'step6') echo 'current-stp'; ?>">6. Указать материальные носители</a></li>
	<li><a href="/doc/step7" class="<? if ($this->router->method > 'step7') echo 'text-success';
		if ($this->router->method == 'step7') echo 'current-stp'; ?>">7. Указать компьютерные программы</a></li>
	<li><a href="/doc/step8" class="<? if ($this->router->method > 'step8') echo 'text-success';
		if ($this->router->method == 'step8') echo 'current-stp'; ?>">8. Указать средства защиты</a></li>
	<li><a href="/doc/step9" class="<? if ($this->router->method > 'step9') echo 'text-success';
		if ($this->router->method == 'step9') echo 'current-stp'; ?>">9. Допустить работников</a></li>

	<li>Опубликовать политику в офисе</li>
	<li>Опубликовать политику на сайте</li>
	<li> Ознакомить работников</li>
	<li>Уведомить Роскомнадзор через интернет</li>
	<li>Уведомить Роскомнадзор по почте</li>
	<li>Провести внутренний контроль</li>
</ul>

<a href="/" class="btn-1">Начать заново</a>
<a href="/doc/generate" class="btn-1">Сгенерировать</a>

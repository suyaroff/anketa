<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2">
			<h2><? echo $title; ?></h2>
			<form action="" method="post">
				<div class="row-2-i">
					<div style="flex-basis: 30%;">
						<? foreach ($company_individuals as $ci) { ?>
							<p id="tab_<? echo $ci->id; ?>" class="tab"><? echo $ci->id; ?>) <? echo $ci->name; ?></p>
						<? } ?>
					</div>
					<div style="flex-basis: 70%;">
						<? foreach ($company_individuals as $ci) { ?>
							<div id="tab_content_<? echo $ci->id; ?>" class="tab_content">
								<? foreach ($third_parties as $tp) { ?>
									<div>
										<label>
											<input type="checkbox" name="third_party_<? echo $ci->id; ?>[]" value="<? echo $tp->title; ?>"
												<? if(is_array($ci->third_party)){if(in_array($tp->title, $ci->third_party)){echo 'checked';}} ?> > <? echo $tp->title; ?></label>
									</div>
								<? } ?>
								<hr>
								<div>
									<label for="allow_world_<? echo $ci->id; ?>" >
										<input type="checkbox" name="allow_world_<? echo $ci->id; ?>" value="1" id="allow_world_<? echo $ci->id; ?>"
											   onchange="$('#country_container_<? echo $ci->id; ?>').toggle();"
										<? if( $ci->allow_world) echo 'checked';?> >данные передаются в другие страны</label>
								</div>
								<div class="country_container" id="country_container_<? echo $ci->id; ?>" style="<? if( !$ci->allow_world) echo 'display:none;';?>">
									<textarea class="textarea" name="countries_<? echo $ci->id; ?>" rows="10" placeholder="Укажите список стран, каждая на новой строке"><?
									 if(is_array($ci->countries)) echo implode("\n", $ci->countries); ?></textarea>
								</div>
								<input type="hidden" name="individual[]" value="<? echo $ci->id; ?>">
							</div>
						<? } ?>
					</div>
				</div>
				<p>
					<button type="submit" class="btn-2">Далее</button>
				</p>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

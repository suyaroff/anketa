<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2">

			<h2><? echo $title; ?></h2>
			<form action="" method="post">

				<? $i = 0;  foreach ($carriers as $c) { $i++;

					if($company_carriers && isset($company_carriers->data[$c->name])) {
						$cc = $company_carriers->data[$c->name];
						unset($company_carriers->data[$c->name]);
					} else {
						$cc = [];
					}

				?>
					<div id="carrier_<?= $i; ?>" class="ca">
						<div class="carrier-title" >
							<input type="checkbox" name="name[<?=$i;?>]" value="<? echo $c->name; ?>" <? if ($cc) echo ' checked ';?> >
							<span class="tu cp" onclick="carrier_show(<?= $i; ?>);"><? echo $c->name; ?></span>
						</div>
						<div class="carrier-body">
							<table style="width: 100%;">
								<tr>
									<td>Название</td>
									<td><b><?= $c->name; ?></b></td>
								</tr>
								<tr>
									<td>Чьи данные содержит?</td>
									<td>
										<? foreach ($company_individuals as $ci) {?>
											<div>
												<label>
													<input type="checkbox" name="individuals_<?=$i;?>[]" value="<? echo $ci->name; ?>"
													<? if($cc && isset($cc['individuals']) && in_array($ci->name, $cc['individuals'])) echo ' checked '; ?>
													> <? echo $ci->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
								<tr>
									<td>Место хранения</td>
									<td><input class="input-text small" name="place_<?=$i;?>" value="<? if($cc) echo $cc['place'];?>" placeholder="Сейф в кабинете 205"></td>
								</tr>
								<tr>
									<td style="vertical-align: top">Какие данные содержит?</td>
									<td>
										<? foreach ($carrier_data as $cd) {?>
											<div>
												<label>
													<input type="checkbox" name="data_<?=$i;?>[]"
														   <? if(isset($cc['data'])){if(in_array($cd->name, $cc['data'])) echo ' checked '; }
														   else { if(array_key_exists($cd->id, $c->default_data)) echo ' checked '; } ?>
														   value="<? echo $cd->name; ?>"  > <? echo $cd->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
							</table>
						</div>
					</div>
				<? } ?>
				<?
				/* отображаем новые */

				if($company_carriers && $company_carriers->data) { foreach ($company_carriers->data as $cc) { $i++; ?>
					<div id="carrier_<?= $i; ?>" class="ca">
						<div class="carrier-title other" >
							<span class="tu cp" onclick="carrier_show(<?= $i; ?>);"><? echo $cc['name']; ?></span>
						</div>
						<div class="carrier-body">
							<table style="width: 100%;">
								<tr>
									<td>Название</td>
									<td>
										<input type="text" class="input-text small" name="name[<?=$i;?>]" value="<? echo $cc['name']; ?>">
									</td>
								</tr>
								<tr>
									<td>Чьи данные содержит?</td>
									<td>
										<? foreach ($company_individuals as $ci) {?>
											<div>
												<label>
													<input type="checkbox" name="individuals_<?=$i;?>[]" value="<? echo $ci->name; ?>"
														<? if(isset($cc['individuals']) && in_array($ci->name, $cc['individuals'])) echo ' checked '; ?>
													> <? echo $ci->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
								<tr>
									<td>Место хранения</td>
									<td><input class="input-text small" name="place_<?=$i;?>" value="<? if($cc) echo $cc['place'];?>" placeholder="Сейф в кабинете 205"></td>
								</tr>
								<tr>
									<td style="vertical-align: top">Какие данные содержит?</td>
									<td>
										<? foreach ($carrier_data as $cd) {?>
											<div>
												<label>
													<input type="checkbox" name="data_<?=$i;?>[]"
													<? if(isset($cc['data']) && in_array($cd->name, $cc['data'])) echo ' checked ';?>
													 value="<? echo $cd->name; ?>"  > <? echo $cd->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
								<tr>
									<td></td>
									<td class="tar">
										<span class="btn-4" onclick="carrierDelete(<?=$i;?>);">Удалить материальный носитель</span>
									</td>
								</tr>
							</table>
						</div>
					</div>
				<? }} ?>
				<? /* создать новое */ $i++;  ?>
				<div id="carrier_<?= $i; ?>" class="ca">
					<div class="carrier-title new" >

						<span class="tu cp" onclick="carrier_show(<?= $i; ?>);">Еще один вариант</span>
					</div>
					<div class="carrier-body">
						<table style="width: 100%;">
							<tr>
								<td>Название</td>
								<td>
									<input type="text" class="input-text small" name="name[<?=$i;?>]" value="">
								</td>
							</tr>
							<tr>
								<td>Чьи данные содержит?</td>
								<td>
									<? foreach ($company_individuals as $ci) {?>
										<div>
											<label>
												<input type="checkbox" name="individuals_<?=$i;?>[]" value="<? echo $ci->name; ?>"> <? echo $ci->name; ?>
											</label>
										</div>
									<? } ?>
								</td>
							</tr>
							<tr>
								<td>Место хранения</td>
								<td><input class="input-text small" name="place_<?=$i;?>" value="" placeholder="Сейф в кабинете 205"></td>
							</tr>
							<tr>
								<td style="vertical-align: top">Какие данные содержит?</td>
								<td>
									<? foreach ($carrier_data as $cd) {?>
										<div>
											<label>
												<input type="checkbox" name="data_<?=$i;?>[]" value="<? echo $cd->name; ?>"  > <? echo $cd->name; ?>
											</label>
										</div>
									<? } ?>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<p>
					<button type="submit" class="btn-2">Далее</button>
				</p>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

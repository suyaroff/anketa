<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2">
			<h2><? echo $title; ?></h2>
			<form action="" method="post" class="form-2-col">
				<p class="form-item">
					<label>Краткое наименование компании</label>
					<input type="text" name="short_company" value="<? echo htmlentities($company->short_company); ?>" class="input-text"
						   autocomplete="off" placeholder="Введите краткое наименование" required>
				</p>
				<p class="form-item">
					<label>Полное наименование компании</label>
					<input type="text" name="full_company" value="<? echo htmlentities($company->full_company); ?>" class="input-text"
						   autocomplete="off" placeholder="Введите полное наименование" required>
				</p>
				<p class="form-item">
					<label>Юридический адрес</label>
					<input type="text" name="u_adress" value="<? echo $company->u_adress; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите юридический адрес" required>
				</p>
				<p class="form-item">
					<label>Тип юридического лица</label>
					<select name="type_ul" class="select" required>
						<option value=""> Выберите из списка</option>
						<? foreach ($company_type as $ct) { ?>
							<option value="<? echo $ct->name; ?>" <? if ($company->type_ul == $ct->name) echo 'selected'; ?> > <? echo $ct->name; ?></option>
						<? } ?>
					</select>
				</p>
				<p class="form-item">
					<label>ИНН</label>
					<input type="text" name="inn" value="<? echo $company->inn; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите ИНН организации" required>
				</p>
				<p class="form-item">
					<label>ОГРН</label>
					<input type="text" name="ogrn" value="<? echo $company->ogrn; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите ОГРН организации" required>
				</p>

				<p class="form-item">
					<label>Дата создания компании</label>
					<input type="text" name="date_open_firm" value="<? echo $company->date_open_firm; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите дату создания" required>
				</p>
				<p class="form-item">
					<label>Основной ОКВЭД</label>
					<input type="text" name="okved" value="<? echo $company->okved; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите Основной ОКВЭД" required>
				</p>

				<p class="form-item">
					<label>ФИО Руководителя</label>
					<input type="text" name="name_ceo" value="<? echo $company->name_ceo; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите ФИО Руководителя" required>
				</p>
				<p class="form-item">
					<label>Должность руководителя</label>
					<input type="text" name="position" value="<? echo $company->position; ?>" class="input-text"
						   autocomplete="off" placeholder="Введите должность руководителя" required>
				</p>
				<p class="form-item">
					<button type="submit" class="btn-2">Далее</button>
				</p>

				<p class="form-item">
					<button type="button" class="btn-1" onclick="parser.get(<? echo $company->ogrn; ?>, this);">Заполнить реквизиты автоматически</button>
				</p>

			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

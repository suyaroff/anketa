<? $this->view('header'); ?>
	<header id="header">
		<a href="/" class="logo"><img src="/images/logo.png" alt="Логотип "></a>
	</header>

	<div class="main-box box-step-1">
		<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
		<div class="box-body">
			<form action="" method="post">
				<p class="tac fwb stp-1-description">
					Приведение деятельности<br>
					организации в соответствии с №152-ФЗ<br>
					«О персональных данных»
				</p>
				<p class="ogrn">
		  			<small>огрн</small>
					<input type="text" class="input-text" name="ogrn" value="<? if (isset($ogrn)) echo $ogrn; ?>" placeholder="Введите ОГРН организации" required
						   autocomplete="off">
				</p>
				<? if (isset($error) && !empty($error)) { ?>
					<p class="error tac"><? echo $error; ?></p>
				<? } ?>
				<p class="step-1-checkbox">
					<input class="styled-checkbox" id="styled-checkbox" type="checkbox" value="value2" checked required>
					<label for="styled-checkbox">
						Я принимаю условия передачи информации<br>
						в соответствии с <a href="#">политикой конфиденциальности</a>
					</label>
				</p>
				<p class="tac">
					<button type="submit" class="btn-1">Продолжить</button>
				</p>
			</form>
		</div>
	</div>
<? $this->view('footer'); ?>

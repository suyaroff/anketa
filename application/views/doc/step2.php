<? $this->view('header'); ?>
	<div class="main-box">
		<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
		<div class="box-body">
			<div class="col-1">
				<? $this->view('col-1') ?>
			</div>
			<div class="col-2">
				<h2><? echo $title;?></h2>
				<form action="" method="post" class="f-proxy">
					<p class="fs19">ИТ-Специалист</p>
					<p class="fwb fs14">Укажите программиста, или системного администратора, если он есть в штате.</p>
					<p class="row-3-i">
						<input type="text" name="position_2" value="<? echo $company->position_2; ?>"
							   class="input-text input-position" autocomplete="off" placeholder="Должность" required>
						<input type="text" name="name_person_2" value="<? echo $company->name_person_2; ?>"
							   class="input-text input-name" autocomplete="off" placeholder="ФИО" required>
						<span class="position-description">ИТ-специалист помогает выполнять<br> технические меры защиты данных.</span>
					</p>
					<p class="fs19">Специалист по кадрам</p>
					<p class="fwb fs14">Укажите сотрудника, который ведет кадровое делопроизводство в вашей организации, и его контакты.</p>
					<p class="row-3-i">
						<input type="text" name="position_1" value="<? echo $company->position_1; ?>"
							   class="input-text input-position" autocomplete="off" placeholder="Должность" required>
						<input type="text" name="name_person_1" autocomplete="off" value="<? echo $company->name_person_1; ?>"
							   class="input-text input-name" placeholder="ФИО" required>
						<span class="position-description">Специалист по кадрам будет ответственен<br>за исполнением 152-ФЗ у вас в организации.</span>
					</p>
					<p class="row-3-i">
						<input type="text" name="tel" value="<? echo $company->tel; ?>"
							   class="input-text input-position" autocomplete="off" placeholder="Телефон" required>
						<input type="text" name="email" value="<? echo $company->email; ?>"
							   class="input-text input-name" autocomplete="off" placeholder="Электронная почта">
						<span class="position-description"></span>
					</p>
					<p>
						<button type="submit" class="btn-2">Далее</button>
					</p>
				</form>
			</div>
		</div>
	</div>
<? $this->view('footer'); ?>

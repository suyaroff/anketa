<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2">
			<h2><? echo $title; ?></h2>
			<form action="" method="post">
				<div class="row-2-i">
					<div style="flex-basis: 30%;">
						<? foreach ($company_individuals as $ci) { ?>
							<p id="tab_<? echo $ci->id; ?>" class="tab"><? echo $ci->id; ?>) <? echo $ci->name; ?></p>
						<? } ?>
					</div>
					<div style="flex-basis: 70%;">
						<? foreach ($company_individuals as $ci) { ?>
							<div id="tab_content_<? echo $ci->id; ?>" class="tab_content">
								<? if (isset($individual_targets[$ci->id])) {
									$i = 0;
									$std_target_selected = [];
									foreach ($individual_targets[$ci->id] as $it) {
										$i++;
										$checked = false;
										if (is_array($ci->targets)) {
											if (in_array($it['target_title'], $ci->targets)) {
												$checked = true;
												$std_target_selected[] = $it['target_title'];
											}
										} elseif ($it['default']) {
											$checked = true;
										}
										?>
										<label><input type="checkbox" name="<? echo $ci->id; ?>[]" value="<? echo $it['target_title']; ?>"
												<? if ($checked) echo 'checked'; ?> > <? echo $it['target_title']; ?></label><br>
									<? }
								} ?>
								<hr>
								<div id="some_target_<? echo $ci->id; ?>">
									<? if (is_array($ci->targets)) {
										foreach ($ci->targets as $t) {
											if (!in_array($t, $std_target_selected)) { ?>
												<input type="text" class="input-text small" name="<? echo $ci->id; ?>[]" value="<? echo $t; ?>" autocomplete="off"><br>
											<? }
										}
									} ?>
								</div>
								<p>
									<span class="btn-3" onclick="add_target_field(<? echo $ci->id; ?>)">Добавить цель</span>
								</p>

							</div>
						<? } ?>
					</div>
				</div>
				<p>
					<button type="submit" class="btn-2">Далее</button>
				</p>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

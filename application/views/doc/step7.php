<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2">
			<h2><? echo $title; ?></h2>
			<form action="" method="post">
				<p>
					<label><input type="checkbox" name="internet_allow" value="Данные передаются по сети интернет" <? if($company->internet_allow) echo 'checked';?> >
						Данные передаются по сети интернет</label><br/>
					<label><input type="checkbox" name="local_allow" value="Данные передаются по локальной сети" <? if($company->local_allow) echo 'checked';?> >
						Данные передаются по локальной сети</label><br/>
				</p>
				<div>
					<b>Где хранятся персональные данные?</b><br>
					<label><input type="checkbox" name="base_owner" value="Да" <? if($company->base_owner) echo 'checked';?> onchange="check_base_owner(this)"> На сервере или компьютере нашей компании</label>
				</div>
				<div id="base_program_box" style="<? if($company->base_owner) echo 'display:block;';?>">
					<div>
						<input type="text" name="base_owner_shortname" value="<? echo $company->base_owner_shortname;?>" class="input-text small">
						<span class="label-2">Наименование организации владельца ЦОД</span>
					</div>
					<div>
						<input type="text" name="base_owner_ogrn" value="<? echo $company->base_owner_ogrn; ?>" class="input-text small">
						<span class="label-2">ОГРН владельца ЦОД</span>
					</div>
					<div>
						<input type="text" name="base_owner_inn" value="<? echo $company->base_owner_inn; ?>" class="input-text small">
						<span class="label-2">ИНН владельца ЦОД</span>
					</div>
					<div>
						<input type="text" name="base_owner_uradress" value="<? echo $company->base_owner_uradress; ?>" class="input-text small">
						<span class="label-2">Юридический адрес владельца ЦОД </span>
					</div>
				</div>

				<? $i = 1;

				if($company_program == null) { ?>
					<div id="carrier_<?= $i; ?>" class="ca">
						<div class="carrier-title">
							<span class="tu cp" onclick="carrier_show(<?= $i; ?>);">1С:Предприятие</span>
						</div>
						<div class="carrier-body">
							<table style="width: 100%;">
								<tr>
									<td>Название</td>
									<td><input type="text" class="input-text small" name="name[<?= $i; ?>]" value="1С:Предприятие"></td>
								</tr>
								<tr>
									<td style="vertical-align: top;">Чьи данные содержит?</td>
									<td>
										<? foreach ($company_individuals as $ci) { ?>
											<div>
												<label>
													<input type="checkbox" name="individuals_<?= $i; ?>[]" checked value="<? echo $ci->name; ?>"> <? echo $ci->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top">Какие данные содержит?</td>
									<td>
										<? foreach ($program_data as $pd) { ?>
											<div>
												<label>
													<input type="checkbox" name="data_<?= $i; ?>[]" value="<? echo $pd->name; ?>" checked> <? echo $pd->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
							</table>
						</div>
					</div>
				<? } elseif($company_program) { foreach ($company_program->data as $p) { $i++; ?>
					<div id="carrier_<?= $i; ?>" class="ca">
						<div class="carrier-title">
							<span class="tu cp" onclick="carrier_show(<?= $i; ?>);"><? echo $p['name'];?></span>
						</div>
						<div class="carrier-body">
							<table style="width: 100%;">
								<tr>
									<td>Название</td>
									<td><input type="text" class="input-text small" name="name[<?= $i; ?>]" value="<? echo $p['name'];?>"></td>
								</tr>
								<tr>
									<td style="vertical-align: top;">Чьи данные содержит?</td>
									<td>
										<? foreach ($company_individuals as $ci) { ?>
											<div>
												<label>
													<input type="checkbox" name="individuals_<?= $i; ?>[]" <? if(isset($p['individuals']) && in_array($ci->name, $p['individuals'])) echo ' checked '; ?> value="<? echo $ci->name; ?>"> <? echo $ci->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top">Какие данные содержит?</td>
									<td>
										<? foreach ($program_data as $pd) { ?>
											<div>
												<label>
													<input type="checkbox" name="data_<?= $i; ?>[]" value="<? echo $pd->name; ?>"
														<? if(isset($p['data']) && in_array($pd->name, $p['data'])) echo ' checked ';?>> <? echo $pd->name; ?>
												</label>
											</div>
										<? } ?>
									</td>
								</tr>
							</table>
						</div>
					</div>
				<? }} ?>
				<?
				/* создать новое */
				$i++;
				?>
				<div id="carrier_<?= $i; ?>" class="ca">
					<div class="carrier-title new-program">
						<span class="tu cp" onclick="carrier_show(<?= $i; ?>);">Новая программа</span>
					</div>
					<div class="carrier-body">
						<table style="width: 100%;">
							<tr>
								<td>Название</td>
								<td>
									<input type="text" class="input-text small" name="name[<?= $i; ?>]" value="">
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top;">Чьи данные содержит?</td>
								<td>
									<? foreach ($company_individuals as $ci) { ?>
										<div>
											<label>
												<input type="checkbox" name="individuals_<?= $i; ?>[]" value="<? echo $ci->name; ?>"> <? echo $ci->name; ?>
											</label>
										</div>
									<? } ?>
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top">Какие данные содержит?</td>
								<td>
									<? foreach ($program_data as $pd) { ?>
										<div>
											<label>
												<input type="checkbox" name="data_<?= $i; ?>[]" value="<? echo $pd->name; ?>"> <? echo $pd->name; ?>
											</label>
										</div>
									<? } ?>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<p>
					<button type="submit" class="btn-2">Далее</button>
				</p>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

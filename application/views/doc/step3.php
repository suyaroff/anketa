<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2">
			<h2><? echo $title; ?></h2>
			<form action="" method="post">
				<div class="row-2-i">
					<div class="step-3-col-1">
						<? foreach ($individuals as $p) { ?>
							<p>
								<label>
									<? if (isset($company_individuals[$p->id])) { ?>
										<input type="checkbox" name="individuals[<? echo $p->id; ?>]" value="<? echo $p->title; ?>" checked
										> <? echo $p->id; ?>) <? echo $p->title; ?>
									<? } else { ?>
										<input type="checkbox" name="individuals[<? echo $p->id; ?>]" value="<? echo $p->title; ?>"
											<? if ($p->id == 1 || $p->id == 4) echo 'checked'; ?> > <? echo $p->id; ?>) <? echo $p->title; ?>
									<? } ?>
								</label>
							</p>
						<? } ?>
						<p>Количество обрабатываемых физлиц</p>
					</div>
					<div class="step-3-col-2">
						<? for ($i = 8; $i < 15; $i++) { ?>
							<p>
								<label>
									<? echo $i; ?>) <input type="text" name="individuals[<? echo $i; ?>]" placeholder="Введите тип физ. лица" class="input-text"
										<? if (isset($company_individuals[$i])) echo 'value="' . $company_individuals[$i]->name . '"' ?>>
								</label>
							</p>
						<? } ?>
						<p>

							<label><input type="radio" name="worker_count" value="до 100000" <? if($company->worker_count == 'до 100000' || !$company->worker_count) echo 'checked';?> >до 100000</label><br>
							<label><input type="radio" name="worker_count" value="больше 100000" <? if($company->worker_count == 'больше 100000') echo 'checked';?> >больше 100000</label>
						</p>
					</div>
				</div>
				<button type="submit" class="btn-2">Далее</button>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

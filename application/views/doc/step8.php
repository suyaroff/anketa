<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2 step-8">
			<h2><? echo $title; ?></h2>
			<form action="" method="post">
				<p>
					Средства защиты информации от несанкционированного доступа
					<textarea name="secure_tools_1" rows="2" class="textarea"><?
					if($company->secure_tools_1)
						echo $company->secure_tools_1;
					else echo 'Встроенные средства операционной системы для идентификации и аутентификации'; ?></textarea>
				</p>
				<p>
					Средства регистрации и учета событий
					<textarea name="secure_tools_2" rows="2" class="textarea"><?
						if($company->secure_tools_2)
							echo $company->secure_tools_2;
						else echo 'Встроенные средства операционной системы для регистрации и учета событий'; ?></textarea>
				</p>
				<p>
					Средства антивирусной защиты
					<textarea name="secure_tools_3" rows="2" class="textarea"><? echo $company->secure_tools_3;?></textarea>
				</p>
				<p>
					Средства резервного копирования и восстановления информации
					<textarea name="secure_tools_4" rows="2" class="textarea"><? echo $company->secure_tools_4;?></textarea>
				</p>
				<p>
					Межсетевые экраны и криптографические шлюзы
					<textarea name="secure_tools_5" rows="2" class="textarea"><?
						if($company->secure_tools_5)
							echo $company->secure_tools_5;
						else echo 'Встроенные средства операционной системы для межсетевого экранирования'; ?></textarea>
				</p>
				<p>
					Средства криптографической защиты информации, наименование, регистрационные номера и производители
					<textarea name="secure_tools_6" rows="2" class="textarea"><? echo $company->secure_tools_6;?></textarea>
				</p>
				<p>
					Средства обнаружения и предотвращения вторжений
					<textarea name="secure_tools_7" rows="2" class="textarea"><? echo $company->secure_tools_7;?></textarea>
				</p>
				<p>Средства анализа защищенности
					<textarea name="secure_tools_8" rows="2" class="textarea"><? echo $company->secure_tools_8;?></textarea>
				</p>

				<p>
					<button type="submit" class="btn-2">Далее</button>
				</p>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

<? $this->view('header'); ?>
<div class="main-box">
	<div class="box-header"><h1 class="box-title">Анкета клиента</h1></div>
	<div class="box-body">
		<div class="col-1"><? $this->view('col-1') ?></div>
		<div class="col-2 step-8">
			<h2><? echo $title; ?></h2>
			<form action="" method="post">
				<p>Работники на каких должностях допущены у вас?</p>
				<p><label><input type="checkbox" disabled checked ><? echo $company->position;?></label></p>
				<p><label><input type="checkbox" disabled checked ><? echo $company->position_2;?></label></p>
				<div id="position_massive">
					<? foreach ($company->position_massive as $pm) { ?>
					<p>
						<input type="text" name="position_massive[]" class="input-text small" placeholder="Еще одна должность" value="<? echo $pm;?>" required>
						<span class="cp tu error" onclick="removePositionItem(this)">удалить</span>
					</p>
					<? } ?>
				</div>
				<p><span class="cp tu" onclick="addPositionItem()">Добавить</span></p>
				<br>
				<p>
					<input type="hidden" name="save" value="1">
					<button type="submit" class="btn-2">Далее</button>
				</p>
			</form>
		</div>
	</div>
</div>
<? $this->view('footer'); ?>

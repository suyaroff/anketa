<?php
/**
 * Created by PhpStorm.
 * User: suyar
 * Date: 22.11.2018
 * Time: 21:01
 */

defined('BASEPATH') OR exit('No direct script access allowed');

function date_f($date, $format = "d.m.Y")
{
	$time = strtotime($date);
	if(!$time) return '';
	return date($format, $time);
}

function date_m($date, $format = "Y-m-d")
{
	$time = strtotime($date);
	return date($format, $time);
}



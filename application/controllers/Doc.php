<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doc extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('company');
		if (isset($this->session->company)) {
			$this->company_id = $this->session->company->id;
		}
	}

	protected $company_id = 0;

	protected $data = ['title' => 'Генератор документов'];

	public function index()
	{
		$this->data['ogrn'] = $this->db->where('id', 1)->get('company')->row('ogrn');
		if (isset($_POST['ogrn']) && !empty($_POST['ogrn'])) {
			$ogrn = trim($_POST['ogrn']);
			$this->data['ogrn'] = $ogrn;
			$company = $this->db->where('ogrn', $ogrn)->get('company')->row();
			if ($company) {
				$this->session->company = $company;
				redirect('/doc/step1');
			} else {
				$this->data['error'] = 'Компания не найдена';
			}
		}

		$this->load->view('doc/index', $this->data);
	}

	public function step1()
	{
		$this->data['title'] = 'Шаг 1. Заполнить реквизиты';
		// проверяем наличие компании
		if (!$this->company_id) redirect('/doc');

		$this->data['company'] = $this->company->get($this->company_id);
		$this->data['company_type'] = $this->db->get('company_type')->result();

		if (isset($_POST['short_company']) && !empty($_POST['short_company'])) {
			$data = array_map('trim', $_POST);
			$update = $this->company->update($data, $this->company_id);
			if ($update) {
				redirect('/doc/step2');
			}
		}

		$this->load->view('doc/step1', $this->data);

	}

	public function step2()
	{
		$this->data['title'] = 'Шаг 2. Указать ключевых работников';
		// проверяем наличие компании в
		if (!$this->company_id) redirect('/doc');

		$this->data['company'] = $this->company->get($this->company_id);

		if (isset($_POST['tel'])) {
			$data = array_map('trim', $_POST);
			$update = $this->company->update($data, $this->company_id);
			if ($update) {
				redirect('/doc/step3');
			}
		}
		$this->load->view('doc/step2', $this->data);
	}

	public function step3()
	{
		if (!$this->company_id) redirect('/doc');

		$this->data['title'] = 'Шаг 3. Указать виды физических лиц';

		if (count($_POST)) {
			$this->db->delete('company_individuals', array('company_id' => $this->company_id));
			foreach ($_POST['individuals'] as $k => $v) {
				if (!empty($v)) {
					$data = ['id' => $k, 'name' => trim($v), 'company_id' => $this->company_id];
					$this->db->insert('company_individuals', $data);
				}
			}

			$this->company->update(['worker_count' => $_POST['worker_count']], $this->company_id);
			redirect('/doc/step4');
		}
		$this->data['company'] = $this->company->get($this->company_id);
		$this->data['individuals'] = $this->db->get('individuals')->result();
		$this->data['company_individuals'] = $this->company->individuals_get($this->company_id);
		$this->load->view('doc/step3', $this->data);
	}

	public function step4()
	{
		if (!$this->company_id) redirect('/doc');
		$this->data['title'] = 'Шаг 4. Указать цели получения данных';

		if (count($_POST)) {
			foreach ($_POST as $individual_id => $targets) {
				$data['targets'] = $targets;
				$updated = $this->company->individuals_save($individual_id, $data, $this->company_id);
			}
			if ($updated) {
				redirect('/doc/step5');
			}
		}

		$this->data['company_individuals'] = $this->company->individuals_get($this->company_id);

		$sql = "SELECT it.*, i.title as individual_title, t.title as target_title
				FROM individual_targets it
					    LEFT JOIN individuals i ON it.individual_id = i.id
						LEFT JOIN targets t ON it.target_id = t.id
				ORDER BY individual_id, `default` DESC";
		$query = $this->db->query($sql);
		foreach ($query->result_array() as $row) {
			$row['individual_title'] = trim($row['individual_title']);
			$row['target_title'] = trim($row['target_title']);
			$this->data['individual_targets'][$row['individual_id']][] = $row;
		}

		$this->load->view('doc/step4', $this->data);
	}

	public function step5()
	{
		if (!$this->company_id) redirect('/doc');
		$this->data['title'] = 'Шаг 5. Указать третьи стороны';

		if (count($_POST)) {
			foreach ($_POST['individual'] as $individual_id) {
				$data = [];
				if (isset($_POST['third_party_' . $individual_id])) {
					$data['third_party'] = $_POST['third_party_' . $individual_id];
				} else {
					$data['third_party'] = '';
				}
				$data['allow_world'] = 0;
				$data['countries'] = '';
				if (isset($_POST['allow_world_' . $individual_id])) {
					$data['allow_world'] = 1;
					$data['countries'] = explode("\n", $_POST['countries_' . $individual_id]);
				}

				if ($data) {
					$updated = $this->company->individuals_save($individual_id, $data, $this->company_id);
					if ($updated) {
						redirect('/doc/step6');
					}
				}

			}
		}
		$this->data['company_individuals'] = $this->company->individuals_get($this->company_id);
		$this->data['third_parties'] = $this->db->get('third_parties')->result();

		$this->load->view('doc/step5', $this->data);
	}

	public function step6()
	{
		if (!$this->company_id) redirect('/doc');
		$this->data['title'] = 'Шаг 6. Указать материальные носители';
		$this->load->model('carriers');
		if (count($_POST)) {
			$this->company->save_carriers($_POST, $this->company_id);
			redirect('/doc/step7');
		}
		$this->data['company_individuals'] = $this->company->individuals_get($this->company_id);
		$this->data['carriers'] = $this->carriers->get_list();
		$this->data['carrier_data'] = $this->carriers->get_data_list();
		$this->data['company_carriers'] = $this->company->get_carriers($this->company_id);
		$this->load->view('doc/step6', $this->data);
	}

	public function step7()
	{
		if (!$this->company_id) redirect('/doc');
		$this->load->model('program');

		$this->data['title'] = 'Шаг 7. Указать компьютерные программы';

		if (count($_POST)) {
			$data['base_owner_shortname'] = $_POST['base_owner_shortname'];
			$data['base_owner_ogrn'] = $_POST['base_owner_ogrn'];
			$data['base_owner_inn'] = $_POST['base_owner_inn'];
			$data['base_owner_uradress'] = $_POST['base_owner_uradress'];
			$data['internet_allow'] = isset($_POST['internet_allow']) ? $_POST['internet_allow'] : '';
			$data['local_allow'] = isset($_POST['local_allow']) ? $_POST['local_allow'] : '';
			$data['base_owner'] = isset($_POST['base_owner']) ? $_POST['base_owner'] : '';
			if(!$data['base_owner']){
				$data['base_owner_shortname'] = '';
				$data['base_owner_ogrn'] = '';
				$data['base_owner_inn'] = '';
				$data['base_owner_uradress'] = '';
			}

			$update = $this->company->update($data, $this->company_id);
			if ($update) {
				$this->company->save_program($_POST, $this->company_id);
				redirect('/doc/step8');
			}
		}

		$this->data['company'] = $this->company->get($this->company_id);
		$this->data['company_individuals'] = $this->company->individuals_get($this->company_id);
		$this->data['program_data'] = $this->program->get_data_list();
		$this->data['company_program'] = $this->company->get_program($this->company_id);

		$this->load->view('doc/step7', $this->data);
	}

	public function step8()
	{
		if (!$this->company_id) redirect('/doc');
		$this->data['title'] = 'Шаг 8. Указать средства защиты';
		$this->data['company'] = $this->company->get($this->company_id);
		if (count($_POST)) {
			$data = array_map('trim', $_POST);
			$update = $this->company->update($data, $this->company_id);
			if ($update) {
				redirect('/doc/step9');
			}
		}
		$this->load->view('doc/step8', $this->data);
	}

	public function step9()
	{
		if (!$this->company_id) redirect('/doc');
		$this->data['title'] = 'Шаг 9. Допустить работников';

		if (isset($_POST['save'])) {
			$data = ['position_massive' => []];
			if (isset($_POST['position_massive'])) {
				foreach ($_POST['position_massive'] as $pm) {
					$pm = trim($pm);
					if ($pm) {
						$data['position_massive'][] = $pm;
					}
				}
			}

			$this->company->update($data, $this->company_id);
		}
		$this->data['company'] = $this->company->get($this->company_id);
		$this->load->view('doc/step9', $this->data);
	}


	public function generate()
	{

		$templates = glob(TEMPLATE_DIR . '/*.docx');
		$company = $this->company->get($this->company_id);
		$carriers = $this->company->get_carriers($this->company_id);
		$all_documents = $all_data_documents = [];
		foreach ($carriers->data as $d) {
			$all_documents[] = $d['name'];
			foreach ($d['data'] as $name) {
				$all_data_documents[$name] = $name;
			}
		}

		sort($all_documents);
		sort($all_data_documents);

		$all_name_fz_datalist = $all_target_fz_datalist = [];
		$individuals = $this->company->individuals_get($this->company_id);
		$allow_world = 'Нет';
		foreach ($individuals as $ind) {
			if ($ind->allow_world) $allow_world = 'Да';
			$all_name_fz_datalist[] = $ind->name;
			$name = 'name_fz_datalist_' . $ind->id;
			$$name = $ind->name;
			$target = 'target_fz_datalist_' . $ind->id;
			$$target = $ind->targets;
			if (is_array($ind->targets)) {
				foreach ($ind->targets as $t) {
					$all_target_fz_datalist[$t] = $t;
				}
			}
		}

		sort($all_name_fz_datalist);
		sort($all_target_fz_datalist);

		$program = $this->company->get_program($this->company_id);
		$all_programs = $all_data_programs = [];
		foreach ($program->data as $d) {
			$all_programs[] = $d['name'];
			foreach ($d['data'] as $name) {
				$all_data_programs[$name] = $name;
			}
		}

		$folder = COMPLETE_DIR . '/' . $company->ogrn;
		if (!is_dir($folder)) mkdir($folder, 0755, true);

		foreach ($templates as $template_path) {
			$file_name_array = explode('/', $template_path);
			$file_name = array_pop($file_name_array);

			$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_path);

			foreach ($templateProcessor->getVariables() as $var) {
				if (substr($var, '0', 18) == 'target_fz_datalist') {
					if (isset($$var)) {
						$rows = count($$var);
						$templateProcessor->cloneRow($var, $rows);
						$i = 1;
						foreach ($$var as $item) {
							$templateProcessor->setValue($var . '#' . $i, $item);
							$i++;
						}
					}
				}

				if (substr($var, '0', 16) == 'name_fz_datalist') {

				}

				if ($var == 'allow_world') {
					$templateProcessor->setValue('allow_world', $allow_world);
				}

				if ($var == 'all_target_fz_datalist') {
					$rows = count($all_target_fz_datalist);
					$templateProcessor->cloneRow('all_target_fz_datalist', $rows);
					$i = 1;
					foreach ($all_target_fz_datalist as $t) {
						$templateProcessor->setValue('all_target_fz_datalist#' . $i, $t);
						$i++;
					}
					$templateProcessor->setValue('all_target_fz_datalist#' . $i, '');
				}
				if ($var == 'all_name_fz_datalist') {
					$rows = count($all_name_fz_datalist);
					$templateProcessor->cloneRow('all_name_fz_datalist', $rows);
					$i = 1;
					foreach ($all_name_fz_datalist as $n) {
						$templateProcessor->setValue('all_name_fz_datalist#' . $i, $n);
						$i++;
					}
					$templateProcessor->setValue('all_name_fz_datalist#' . $i, '');
				}

				if ($var == 'all_documents') {

					$rows = count($all_documents);
					$templateProcessor->cloneRow('all_documents', $rows);
					$i = 1;
					foreach ($all_documents as $item) {
						$templateProcessor->setValue('all_documents#' . $i, $item);
						$i++;
					}
				}

				if ($var == 'all_data_documents') {
					$rows = count($all_data_documents);
					$templateProcessor->cloneRow('all_data_documents', $rows);
					$i = 1;
					foreach ($all_data_documents as $n) {
						$templateProcessor->setValue('all_data_documents#' . $i, $n);
						$i++;
					}
					$templateProcessor->setValue('all_data_documents#' . $i, '');
				}

				if ($var == 'all_programs') {
					$rows = count($all_programs);
					$templateProcessor->cloneRow('all_programs', $rows);
					$i = 1;
					foreach ($all_programs as $item) {
						$templateProcessor->setValue('all_programs#' . $i, $item);
						$i++;
					}
				}

				if ($var == 'all_data_programs') {
					$rows = count($all_data_programs);
					$templateProcessor->cloneRow('all_data_programs', $rows);
					$i = 1;
					foreach ($all_data_programs as $item) {
						$templateProcessor->setValue('all_data_programs#' . $i, $item);
						$i++;
					}
				}


				if ($var == 'version') {
					$templateProcessor->setValue('version', DOC_VERSION);
				} elseif ($var == 'date') {
					$templateProcessor->setValue('date', NOW_DATE);
				} elseif ($var == 'year') {
					$templateProcessor->setValue('year', NOW_YEAR);
				} elseif ($var == 'position_massive') {
					$rows = count($company->position_massive) + 2;
					$templateProcessor->cloneRow('position_massive', $rows);
					$templateProcessor->setValue('position_massive#1', $company->position);
					$templateProcessor->setValue('position_massive#2', $company->position_2);
					$i = 3;
					foreach ($company->position_massive as $pm) {
						$templateProcessor->setValue('position_massive#' . $i, $pm);
						$i++;
					}
					$templateProcessor->setValue('position_massive#' . $i, '');
				} elseif (isset($company->$var)) {
					$templateProcessor->setValue($var, $company->$var);
				}
			}

			$generated_path = $folder . '/' . $file_name;
			$url = '/complete/' . $company->ogrn . '/' . $file_name;
			$templateProcessor->saveAs($generated_path);

			//echo '<p><a href="' . $url . '">' . $file_name . '</a></p>';
		}
		$zip = new ZipArchive();
		$zip_file = $company->short_company . '.zip';
		$ret = $zip->open(COMPLETE_DIR . '/' . $zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
		if ($ret !== TRUE) {
			printf('Ошибка с кодом %d', $ret);
		} else {
			$options = array('remove_all_path' => TRUE);
			chdir($folder);
			$zip->addGlob('*.docx', GLOB_BRACE, $options);
			$zip->close();
			header('Location:/complete/' . $zip_file);
		}

	}

	public function test()
	{
		$this->company->all_data_documents(1);
	}

	public function parse($ogrn)
	{

		$this->data = [
			'error' => '',
			'source_url' => 'https://www.rusprofile.ru/search?query=' . $ogrn
		];

		$d = file_get_contents($this->data['source_url']);

		//$d = file_get_contents(FCPATH.'html_source.txt');
		if (strlen($d) > 30000) {
			preg_match('#<h1 itemprop="name">.*?</h1>#s', $d, $m);
			if (isset($m[0])) $this->data['short_company'] = trim(strip_tags(html_entity_decode($m[0])));

			preg_match('#<div class="col-sm-10" itemprop="legalName">.*?</div>#s', $d, $m);
			if (isset($m[0])) $this->data['full_company'] = trim(strip_tags(html_entity_decode($m[0])));

			preg_match('#itemprop="address" .*?>(.*?)</div>#s', $d, $m);
			if (isset($m[1])) {
				$this->data['u_adress'] = trim(strip_tags($m[1]));
				$this->data['u_adress'] = str_replace("\n", " ", $this->data['u_adress']);
				$this->data['u_adress'] = str_replace("  ", "", $this->data['u_adress']);
			}


			preg_match('#data-ogrn="(\d*)"#', $d, $m);
			if (isset($m[1])) $this->data['ogrn'] = trim(strip_tags($m[1]));

			preg_match('#data-inn="(\d*)"#', $d, $m);
			if (isset($m[1])) $this->data['inn'] = trim(strip_tags($m[1]));

			preg_match('#<div class="col-left dl" itemprop="jobTitle">(.*?)</div>#s', $d, $m);
			if (isset($m[1])) $this->data['position'] = trim(strip_tags($m[1]));

			preg_match('#jobTitle.*?<span itemprop="name">(.*?)<br>#s', $d, $m);
			if (isset($m[1])) $this->data['name_ceo'] = trim(strip_tags($m[1]));

			preg_match('#Основной вид деятельности.*?code.*?<span>(.*?)</span></div>.*?name">(.*?)</div>#s', $d, $m);

			if (isset($m[1]) && isset($m[2])) {
				$m[1] = trim(strip_tags($m[1]));
				$m[2] = trim(strip_tags($m[2]));
				$this->data['okved'] = $m[1] . ', ' . $m[2];
			}

			preg_match('#foundingDate" content="(.*)"#', $d, $m);

			if (isset($m[1])) {
				$m[1] = trim(strip_tags($m[1]));
				$this->data['date_open_firm'] = date_f($m['1']);
			}


		} else {
			$this->data['error'] = "По ОГРН $ogrn ничего не найдено";
		}

		echo json_encode($this->data);
	}


}

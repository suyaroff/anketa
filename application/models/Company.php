<?php

class Company extends CI_Model
{

	public $id = 0;

	/**
	 * Типы физ лиц
	 * @param $company_id
	 * @return array
	 */
	public function individuals_get($company_id)
	{
		$data = [];
		$query = $this->db->where('company_id', $company_id)->get('company_individuals');
		foreach ($query->result() as $row) {
			$row->targets = json_decode($row->targets);
			$row->third_party = json_decode($row->third_party);
			$row->countries = json_decode($row->countries);
			$data[$row->id] = $row;
		}
		return $data;
	}

	/**
	 * Сохранение фих лиц
	 * @param $id
	 * @param $data
	 * @param $company_id
	 * @return mixed
	 */
	public function individuals_save($id, $data, $company_id)
	{

		if (isset($data['targets'])) {
			if (is_array($data['targets'])) {
				$target = [];
				foreach ($data['targets'] as $t) {
					if ($t = trim($t)) {
						$target[] = $t;
					}
				}
				$data['targets'] = json_encode($target);
			} else {
				$data['targets'] = '';
			}

		}

		if (isset($data['third_party'])) {
			if (is_array($data['third_party'])) {
				$data['third_party'] = json_encode($data['third_party']);
			} else {
				$data['third_party'] = '';
			}

		}
		if (isset($data['countries'])) {
			if (is_array($data['countries'])) {
				$data['countries'] = json_encode($data['countries']);
			} else {
				$data['countries'] = '';
			}
		}

		$updated = $this->db->update('company_individuals', $data, array('id' => $id, 'company_id' => $company_id));
		return $updated;
	}


	/**
	 * Компания
	 * @param $id
	 * @return mixed
	 */
	public function get($id)
	{
		$row = $this->db->where('id', $id)->get('company')->row();
		$row->date_open_firm = date_f($row->date_open_firm);
		$row->position_massive = json_decode($row->position_massive);
		$this->id = $row->id;
		return $row;
	}

	/**
	 * Обновление компании
	 * @param $data
	 * @param $id
	 * @return mixed
	 */
	public function update($data, $id)
	{
		if (isset($data['date_open_firm'])) $data['date_open_firm'] = date_m($data['date_open_firm']);
		if (isset($data['position_massive'])) $data['position_massive'] = json_encode($data['position_massive']);
		$updated = $this->db->update('company', $data, array('id' => $id));
		return $updated;
	}

	public function get_carriers($company_id)
	{
		$row = $this->db->where('company_id', $company_id)->get('company_carriers')->row();
		if ($row) {
			$row->data = json_decode($row->json_data, true);
		}
		return $row;
	}

	public function save_carriers($input, $company_id)
	{

		$this->db->delete('company_carriers', array('company_id' => $company_id));
		$data = ['company_id' => $company_id, 'json_data' => []];
		if (isset($input['name'])) {
			foreach ($input['name'] as $idx => $name) {
				$name = trim($name);
				if ($name) {
					$carrier = ['name' => $name];
					// чьи данные
					if (isset($_POST['individuals_' . $idx])) {
						foreach ($_POST['individuals_' . $idx] as $individual) {
							$carrier['individuals'][] = $individual;
						}
					}
					// место
					if (isset($_POST['place_' . $idx])) {
						$carrier['place'] = $_POST['place_' . $idx];
					}
					// какие данные
					if (isset($_POST['data_' . $idx])) {
						foreach ($_POST['data_' . $idx] as $some_data) {
							$carrier['data'][] = $some_data;
						}
					}
					$data['json_data'][$name] = $carrier;

				}
			}
		}

		$data['json_data'] = json_encode($data['json_data']);

		$this->db->insert('company_carriers', $data);

	}

	public function get_program($company_id)
	{
		$row = $this->db->where('company_id', $company_id)->get('company_program')->row();
		if ($row) {
			$row->data = json_decode($row->json_data, true);
		}
		return $row;
	}

	public function save_program($input, $company_id)
	{

		$this->db->delete('company_program', array('company_id' => $company_id));
		$data = ['company_id' => $company_id, 'json_data' => []];
		if (isset($input['name'])) {
			foreach ($input['name'] as $idx => $name) {
				$name = trim($name);
				if ($name) {
					$program = ['name' => $name];
					// чьи данные
					if (isset($_POST['individuals_' . $idx])) {
						foreach ($_POST['individuals_' . $idx] as $individual) {
							$program['individuals'][] = $individual;
						}
					}

					// какие данные
					if (isset($_POST['data_' . $idx])) {
						foreach ($_POST['data_' . $idx] as $some_data) {
							$program['data'][] = $some_data;
						}
					}
					$data['json_data'][$name] = $program;

				}
			}
		}

		$data['json_data'] = json_encode($data['json_data']);

		$this->db->insert('company_program', $data);

	}


	public function all_data_documents($company_id)
	{
		$data = [];
		$carrier = $this->get_carriers($company_id);
		foreach ($carrier->data as $c) {
			$data[] = $c['name'];
		}
		return $data;
	}


}

<?php

class Program extends CI_Model
{

	function get_data_list() {
		$data = [];
		$query = $this->db->get('program_data');
		foreach ($query->result() as $c) {
			$data[$c->id] = $c;
		}
		return $data;
	}
}

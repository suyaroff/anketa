<?php

class Carriers extends CI_Model
{
	function get_list() {
		$data = [];
		$query = $this->db->get('carriers');
		foreach ($query->result() as $c) {
			$c->default_data = $this->get_data_default($c->id);
			$data[$c->id] = $c;
		}

		return $data;
	}

	function get_data_list() {
		$data = [];
		$query = $this->db->get('carrier_data');
		foreach ($query->result() as $c) {
			$data[$c->id] = $c;
		}
		return $data;
	}

	function get_data_default($carrier_id) {
		$data = [];
		$query = $this->db->where('carrier_id' , $carrier_id)->get('carriers_data_default');
		foreach ($query->result() as $c) {
			$data[$c->data_id] = $c;
		}
		return $data;
	}
}

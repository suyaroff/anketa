var parser = {
	'get': function (ogrn, b) {
		$(b).html('Поиск по ОГРН').prop('disabled', 1);
		$.get('/doc/parse/' + ogrn, function (data) {
			$(b).html('Заполнить реквизиты автоматически').prop('disabled', 0);
			data = JSON.parse(data);
			if (data.error) {
				alert(data.error);
			} else {
				$('.form-item .input-text').each(function (k, v) {
					if (data[v.name]) {
						v.value = data[v.name];
					}
				})
			}
		});
	}
};

function add_target_field(id) {
	var field = '<input type="text" class="input-text small" name="'+id+'[]" placeholder="Введите описание цели" autocomplete="off"><br>';
	$('#some_target_'+id).append(field);
}

function init_tab() {
	var first = {};
	$('.tab').each(function (i, o) {
		if(i == 0) first = o;
		$(o).on('click', function () {
			var indId = this.id.split('_')[1];
			$('.tab').removeClass('active');
			$(this).addClass('active');
			$('.tab_content').hide();
			$('#tab_content_'+indId).show()
		})
	});
	$(first).trigger('click');
}

function activate_carrier(idx) {
	var carrier_template = $(el).val();
	if($(el).prop('checked')) {
		//$('#carrier_'+carrier_template+' .carrier-body input').prop('disabled', false);
	} else {
		//$('#carrier_'+carrier_template+' .carrier-body input').prop('disabled', true);
	}
}

function carrier_show(id) {
	$('.carrier-body').slideUp();
	if(!$('#carrier_'+id+' .carrier-body').is(':visible')){
		$('#carrier_'+id+' .carrier-body').slideDown();
	}
}

function carrierDelete(i) {
	if(confirm('Удалить ?')){
		$('#carrier_'+i).remove();
	}
}

function removePositionItem(el) {
	$(el).parent().remove();
}

function addPositionItem() {
	var el = '<p><input type="text" name="position_massive[]" class="input-text small" placeholder="Еще одна должность" value="" required>' +
		'<span class="cp tu error" onclick="removePositionItem(this)">удалить</span></p>'
	$('#position_massive').append(el);
}

function check_base_owner(el) {
	if(el.checked){
		$('#base_program_box').show()
	} else {
		$('#base_program_box').hide()
	}
}

init_tab();
